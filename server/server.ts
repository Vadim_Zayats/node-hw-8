import express, { Express } from "express";
import morgan from "morgan";
import cors from "cors";
import fs from "fs";
import path from "path";
import newspostsConfigs from "./routes/newspost";
import { errorHandler } from "./services/errorHandler";

const logsDir = path.join(__dirname, "logs");
if (!fs.existsSync(logsDir)) {
  fs.mkdirSync(logsDir);
}
export const accessLogStream = fs.createWriteStream(
  path.join(logsDir, "access.log"),
  {
    flags: "a",
  }
);

class Server {
  app: Express;
  HOST: string;
  PORT: number;

  constructor() {
    this.app = express();
    this.HOST = process.env.HOST || "localhost";
    this.PORT = Number(process.env.PORT) || 8000;
    this.configureMiddleware();
    this.configureRoutes();
    this.configureErrorHandling();
  }

  private configureMiddleware(): void {
    this.app.use(cors());
    // Логування для morgan
    this.app.use(
      morgan(
        (tokens, req, res) => {
          return [
            `HTTP метод: ${tokens.method(req, res)}`,
            `URL запиту: ${tokens.url(req, res)}`,
            `Статус запиту: ${tokens.status(req, res)}`,
            `Тіло запиту: ${JSON.stringify(req.body)}`,
          ].join(" ");
        },
        { stream: accessLogStream }
      )
    );
    // Потік для виведення журналу у консоль
    this.app.use(
      morgan("dev", {
        stream: process.stdout, // Process.stdout для виведення в консоль
      })
    );
  }

  private configureRoutes(): void {
    this.app.use("/api/newsposts/", newspostsConfigs);
    this.app.use("/api/newsposts/:id", newspostsConfigs);
  }

  private configureErrorHandling(): void {
    this.app.use(errorHandler);
  }

  public start(): void {
    this.app.listen(this.PORT, () => {
      console.log(`Server is running on port ${this.PORT}`);
    });
  }
}

const server = new Server();
server.start();

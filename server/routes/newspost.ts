import express, { Router, Request, Response } from "express";
import bodyParser from "body-parser";
import NewsPostController from "../controllers/newspost";
import { tryCatch } from "../services/trycatch";

class PostRouter {
  router: Router;

  constructor() {
    this.router = express.Router();
    this.config();
  }

  private config(): void {
    this.router.use(bodyParser.json());
    this.router
      .route("/")
      .get(tryCatch(NewsPostController.getAllPosts))
      .post(tryCatch(NewsPostController.createNewPost));

    this.router
      .route("/:id")
      .get(tryCatch(NewsPostController.getPostById))
      .put(tryCatch(NewsPostController.editPost));
  }
}

export default new PostRouter().router;

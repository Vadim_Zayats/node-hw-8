import { Request, Response, NextFunction } from "express";
import { accessLogStream } from "../server";

export class ValidationError extends Error {
  constructor(message: string) {
    super(message);
    this.name = "ValidationError";
  }
}

export class NewspostsServiceError extends Error {
  constructor(message: string) {
    super(message);
    this.name = "NewspostsServiceError";
  }
}

export const errorHandler = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  console.log(err.message);
  if (err.name === "ValidationError") {
    accessLogStream.write(`Validation Error: ${err.message}\n`);
    return res.status(400).send("Помилка валідації: " + err.message);
  } else if (err.name === "NewspostsServiceError") {
    accessLogStream.write(`Newsposts Service Error: ${err.message}\n`);
    return res.status(500).send("Помилка сервісу: " + err.message);
  } else {
    accessLogStream.write(`Unknown error: ${err.message}\n`);
    return res.status(500).send("Невідома помилка");
  }
};

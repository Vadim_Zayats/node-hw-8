import * as fsPromise from "fs/promises";
import { Random } from "random-js";
import {
  Data,
  Schema,
  InputData,
  UpdatedData,
  GetTableOperations,
} from "../interfaces/interfaces";

export class FileDB {
  private schemas: { [key: string]: Schema } = {};
  private static instance: FileDB;

  private constructor() {}

  static getInstance(): FileDB {
    if (!FileDB.instance) {
      FileDB.instance = new FileDB();
    }
    return FileDB.instance;
  }

  private async saveDataToFile(filePath: string, data: Data[]): Promise<void> {
    try {
      await fsPromise.writeFile(filePath, JSON.stringify(data, null, 2));
      console.log(`Дані збережено у файлі ${filePath}`);
    } catch (error) {
      console.error(`Помилка при збереженні даних у файлі ${filePath}:`, error);
    }
  }

  private async readDataFromFile(
    filePath: string
  ): Promise<Data[] | undefined> {
    try {
      const data = await fsPromise.readFile(filePath, "utf-8");
      if (data !== undefined) {
        return JSON.parse(data);
      } else {
        console.log("Помилка: не вдалося прочитати файл.");
      }
    } catch (error) {
      console.error(error);
    }
  }

  async registerSchema(tableName: string, schema: Schema): Promise<void> {
    this.schemas[tableName] = schema;
    const filePath = `${tableName}.json`;
    try {
      await fsPromise.writeFile(filePath, JSON.stringify([], null, 2));
      console.log(`Створено файл бази даних ${tableName}`);
    } catch (error) {
      console.error(
        `Помилка при створенні файлу бази даних ${tableName}:`,
        error
      );
    }
  }

  async saveData(tableName: string, data: Data[]): Promise<void> {
    const filePath = `${tableName}.json`;
    await this.saveDataToFile(filePath, data);
  }

  async readData(tableName: string): Promise<Data[] | undefined> {
    const filePath = `${tableName}.json`;
    const gottedData: Data[] | undefined = await this.readDataFromFile(
      filePath
    );
    const timedData = gottedData?.map(({ createDate, ...other }) => {
      return { ...other, createDate: new Date(createDate) };
    });
    return timedData;
  }

  getTable(tableName: string): GetTableOperations {
    return new GetTableOperationsInstance(tableName);
  }
}

class GetTableOperationsInstance implements GetTableOperations {
  private tableName: string;

  constructor(tableName: string) {
    this.tableName = tableName;
  }

  async getAll(
    page: number,
    size: number
  ): Promise<{ paginatedData: Data[]; totalLength: number } | undefined> {
    try {
      const allData = await fileDB.readData(this.tableName);
      if (allData !== undefined) {
        const startIndex = (page - 1) * size;
        const paginatedData = allData.slice(startIndex, startIndex + size);
        return { paginatedData, totalLength: allData.length };
      } else {
        return undefined;
      }
    } catch (error) {
      console.error("Error fetching all data:", error);
      return undefined;
    }
  }

  async getById(id: number): Promise<Data | undefined> {
    const allData = await fileDB.readData(this.tableName);
    if (allData !== undefined) {
      return allData.find((item: Data) => item.id === id);
    } else {
      return undefined;
    }
  }

  async create(data: InputData): Promise<Data> {
    let db = await fileDB.readData(this.tableName);
    if (!db) {
      db = [];
    }

    const newData: Data = {
      id: new Random().integer(10000, 99999),
      ...data,
      createDate: new Date(),
    };

    db.push(newData);

    await fileDB.saveData(this.tableName, db);
    return newData;
  }

  async update(
    id: number,
    newData: Partial<UpdatedData>
  ): Promise<Data | undefined> {
    let allData: Data[] | undefined = await fileDB.readData(this.tableName);
    if (allData !== undefined) {
      const targetData = allData.find((item: Data) => item.id === id);
      if (targetData) {
        const updatedData = {
          ...targetData,
          ...newData,
        };
        // Оновлюємо елемент у масиві
        allData = allData.map((item: Data) =>
          item.id === id ? updatedData : item
        );
        await fileDB.saveData(this.tableName, allData);
        return updatedData;
      }
    }
  }

  async delete(id: number): Promise<number | undefined | null> {
    let allData = await fileDB.readData(this.tableName);
    if (allData !== undefined) {
      const indexToDelete = allData.findIndex(
        (item: Data | undefined) => item && item.id === id
      );
      if (indexToDelete !== -1) {
        const deletedId = allData[indexToDelete].id;
        allData = allData.filter(
          (item: Data | undefined) => item && item.id !== id
        );
        await fileDB.saveData(this.tableName, allData);
        console.log(`Пост з id "${id}" видалено`);
        return deletedId;
      }
      console.log(`Посту з id "${id}" не існує`);
      return null;
    }
  }
}

export const fileDB = FileDB.getInstance();

export const newspostTable = fileDB.getTable("newsposts");

const newspostSchema = {
  id: Number,
  title: String, // max 50 characters
  text: String, // max 256 characters
  genre: String, // support only 'Politic', 'Business', 'Sport', 'Other'
  isPrivate: Boolean, // ony true or false
  createDate: Date,
};

(async () => {
  fileDB.registerSchema("newsposts", newspostSchema);
  await newspostTable.create({
    title: "Title 0",
    text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste, earum accusamus id necessitatibus iusto voluptate expedita cumque minus odit ea autem?",
    genre: "Politic",
    isPrivate: true,
  });
  await newspostTable.create({
    title: "Title 1",
    text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste, earum accusamus id nece.",
    genre: "Politic",
    isPrivate: true,
  });
  await newspostTable.create({
    title: "Title 2",
    text: "Lorem ipsum dolor sit amet consectetur adipisicing iusto voluptate expedita cumque minus odit ea autem cusamus id necessitatibus iusto voluptate expedita cumque minus?",
    genre: "Politic",
    isPrivate: true,
  });
  await newspostTable.create({
    title: "Title 3",
    text: "Lorem ipsum dolor sit amet accusamus id necessitatibus iusto voluptate expedita cumque minus odit ea autem.",
    genre: "Politic",
    isPrivate: true,
  });
  await newspostTable.create({
    title: "Title 4",
    text: "Lorem ipsum dolor sit amet accusamus id necessitatibus iusto voluptate expedita cumque minus odit ea autem consectetur adipisicing iusto voluptate expedita cumque minus odit ea autem?",
    genre: "Politic",
    isPrivate: true,
  });
  await newspostTable.create({
    title: "Title 5",
    text: "Lorem ipsum dolor sit amet accusamus consectetur adipisicing iusto voluptate expedita cumque minus odit ea autem id necessitatibus iusto voluptate expedita cumque minus odit ea autem.",
    genre: "Politic",
    isPrivate: true,
  });
})();

export interface GetTableOperations {
  getAll: (
    page: number,
    size: number
  ) => Promise<{ paginatedData: Data[]; totalLength: number } | undefined>;
  getById: (id: number) => Promise<Data | undefined>;
  create: (dataArray: InputData) => Promise<Data>;
  update: (
    id: number,
    newData: Partial<Data>
  ) => Promise<Data | string | undefined>;
  delete: (id: number) => Promise<number | undefined | null>;
}

export interface Data {
  id: number;
  title: string; // max 50 characters
  text: string; // max 256 characters
  genre: string; // support only 'Politic', 'Business', 'Sport', 'Other'
  isPrivate: boolean; // ony true or false
  createDate: Date;
}

export interface Schema {
  [key: string]: any;
}

export interface InputData {
  title: string;
  text: string;
  genre: string;
  isPrivate: boolean;
}

// frontend

export interface UpdatedData {
  title: string;
  text: string;
  genre: string;
  isPrivate: boolean;
}
